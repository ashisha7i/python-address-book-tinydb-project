from flask import Flask, render_template, request
from tinydb import TinyDB, Query
import uuid

server = Flask(__name__)

# Initializing the database with the name 'contacts.db' (This can be any name that you choose)
db = TinyDB("contacts.db")

@server.route("/")
@server.route("/home")
def home_page():
    return render_template("home.html")


@server.route("/add")
def add_contact():
    return render_template("add.html")


@server.route("/list")
def list_contacts():
    # Since the data is in the DB now. We need to fetch the data from the database and set a local variable.
    # We will refer to this variable in the template for displaying the contacts.
    contacts_list = db.all()
    num_of_contacts = len(contacts_list)

    # Note the use of '**locals()' here.
    return render_template("list.html", **locals())


@server.route("/about")
def show_about():
    return render_template("about.html")

@server.route("/delete/<contact_id>")
def delete_contact(contact_id):
    contact = Query()
    db.remove(contact.id == contact_id)
    
    # Creating a variable to use in the confirm template to show the operation ('Deleted' here)
    operation = "Deleted"
    return render_template("confirmation.html", **locals())
    
    

@server.route("/confirm", methods=["POST"])
def show_confirmation():
    contact = {}  # Initializing an empty dictionary for this contact
    contact['id'] = str(uuid.uuid4())
    contact['first_name'] = request.form.get('first_name')
    contact['last_name'] = request.form.get('last_name')
    contact['email'] = request.form.get('email')
    contact['phone'] = request.form.get('phone')

    # Save the contact in the Database
    db.insert(contact)
    
    # Creating a variable to use in the confirm template to show the operation ('Added' here)
    operation = "Added"

    return render_template("confirmation.html", **locals())




# ---- Running the server ----
if __name__ == '__main__':
    server.run(debug=True)